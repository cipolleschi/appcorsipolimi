﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Navigation;
using Microsoft.Phone.Controls;
using Microsoft.Phone.Shell;
using Test.ViewModel;
using Test.Services;
using Test.Models;

namespace Test.Pages
{
    public partial class ProductsPage : PhoneApplicationPage
    {
        private ProductsPageVM _dataContext;
        private DateTime _startTime;

        public ProductsPage()
        {
            InitializeComponent();
        }

        protected override void OnNavigatedTo(NavigationEventArgs e)
        {
            base.OnNavigatedTo(e);
            GoogleAnalytics.EasyTracker.GetTracker().SendView("ProductsPage");
            _startTime = DateTime.Now;
            if (NavigationContext.QueryString.ContainsKey("category"))
            {
                _dataContext = new ProductsPageVM();
                string category_key =  NavigationContext.QueryString["category"];
                var c = MemoryManager.Instance.Categories.Where(x => x.Id == category_key).SingleOrDefault();
                _dataContext.Category = c.Name;
                var l = MemoryManager.Instance.Products.Where(x => x.CategoryId == category_key).ToList<Product>();
                GoogleAnalytics.EasyTracker.GetTracker().SendEvent("products_found", "Search", category_key, l.Count);
                foreach (var p in l)
                {
                    _dataContext.Products.Add(new ComplexObjectListVM()
                    {
                        Id = p.Id,
                        Line1 = p.Name,
                        Line2 = String.Format("{0} {1}", p.Brand, p.Model),
                        Value1 = p.Rate,
                        Value2 = p.Price
                    });
                }
                DataContext = _dataContext;
            }
            else
            {
                GoogleAnalytics.EasyTracker.GetTracker().SendException("category not found", false);
            }
        }

        protected override void OnNavigatedFrom(NavigationEventArgs e)
        {
            GoogleAnalytics.EasyTracker.GetTracker().SendTiming(DateTime.Now.Subtract(_startTime), "permanenza", "ProductsPage", "Navigation");
            base.OnNavigatedFrom(e);
        }

        private void Grid_Tap(object sender, System.Windows.Input.GestureEventArgs e)
        {
            Grid g = sender as Grid;
            var uriString = String.Format("/Pages/DetailsPage.xaml?id={0}", g.Tag.ToString());
            GoogleAnalytics.EasyTracker.GetTracker().SendEvent(g.Tag.ToString(), "Click", "Product Selection", 0);
            NavigationService.Navigate(new Uri(uriString, UriKind.Relative));
        }
    }
}