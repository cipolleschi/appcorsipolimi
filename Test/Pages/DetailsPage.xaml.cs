﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Navigation;
using Microsoft.Phone.Controls;
using Microsoft.Phone.Shell;
using Test.ViewModel;
using Test.Services;
using System.Threading;
using GoogleAnalytics.Core;
using GoogleAnalytics;

namespace Test.Pages
{
    public partial class DetailsPage : PhoneApplicationPage
    {
        private DetailsPageVM _dataContext;
        private DateTime _startTime;
        private string id;
        private string categoryId;

        public DetailsPage()
        {
            InitializeComponent();
        }

        protected override void OnNavigatedTo(NavigationEventArgs e)
        {
            base.OnNavigatedTo(e);
            GoogleAnalytics.EasyTracker.GetTracker().SendView("DetailsPage");
            _startTime = DateTime.Now;
            if (NavigationContext.QueryString.ContainsKey("id"))
            {
                id = NavigationContext.QueryString["id"].ToString();
                var item = MemoryManager.Instance.Products.Where(x => x.Id == id).SingleOrDefault();
                if (item != null)
                {
                    _dataContext = new DetailsPageVM()
                    {
                        CurrentRating = 0,
                        Name = item.Name,
                        Brand = item.Brand,
                        Description = item.Description,
                        Model = item.Model,
                        Price = item.Price,
                        Subcategory = item.Subcategory,
                        Rating = item.Rate
                    };
                    categoryId = item.CategoryId;
                    DataContext = _dataContext;
                }
                else
                {
                    GoogleAnalytics.EasyTracker.GetTracker().SendException("Product not found", false);
                }
                
            }
            else
            {
                GoogleAnalytics.EasyTracker.GetTracker().SendException("Product id not found", false);
            }
        }

        protected override void OnNavigatedFrom(NavigationEventArgs e)
        {
            GoogleAnalytics.EasyTracker.GetTracker().SendTiming(DateTime.Now.Subtract(_startTime), "permanenza", "DetailsPage", "Navigation");
            base.OnNavigatedFrom(e);
        }

        private void MinusButton_Click(object sender, RoutedEventArgs e)
        {
            if (_dataContext.CurrentRating <= 0)
            {
                _dataContext.CurrentRating = 0;
            }
            else
            {
                _dataContext.CurrentRating--;
            }
        }

        private void PlusButton_Click(object sender, RoutedEventArgs e)
        {
            if (_dataContext.CurrentRating >= 5)
            {
                _dataContext.CurrentRating = 5;
            }
            else
            {
                _dataContext.CurrentRating++;
            }
        }

        private void RateBtn_Click(object sender, RoutedEventArgs e)
        {
            Timer dt = new Timer((state) =>
            {
                GoogleAnalytics.EasyTracker.GetTracker().SendEvent(id, "Click", "Rate", _dataContext.CurrentRating);
                Dispatcher.BeginInvoke(() =>
                {
                    MessageBox.Show("Votazione inviata con successo!");
                });
            }, null, 1000, -1); 
            
        }

       

        private void BuyBtn_Click(object sender, RoutedEventArgs e)
        {
            Timer dt = new Timer(MyTimerCallback, null, 1000, -1); 
        }

        private void MyTimerCallback(object state)
        {
            
            Transaction myTrans = new Transaction(Guid.NewGuid().ToString(),(long)(_dataContext.Price * 1000000))
            {
                Affiliation = "In-App Purchase", // (string) Affiliation
                TotalTaxInMicros = (long)(0.20 * 1000000), // (long) Total tax (in micros)
                ShippingCostInMicros = 0 // (long) Total shipping cost (in micros)
            };

            myTrans.Items.Add(new TransactionItem(
                id, // (string) Product SKU
                string.Format("{0}-{1}, {2}", _dataContext.Name, _dataContext.Model, _dataContext.Brand),// (string) Product name
                (long)(_dataContext.Price * 1000000), // (long) Product price (in micros)
                (long)1) // (long) Product quantity
            {
                Category = categoryId // (string) Product category
            });

            EasyTracker.GetTracker().SendTransaction(myTrans); // Send the transaction.
            
            Dispatcher.BeginInvoke(() =>
            {
                MessageBox.Show("Acquisto effettuato con successo!");
            });
            
        }

        private void ShareWithFacebook()
        {
            //Do something
            EasyTracker.GetTracker().SendSocial("facebook", "share", id);
        }
    }
}