﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Test.Models
{
    public class Product
    {
        public string Name { get; set; }
        public string Model { get; set; }
        public string Brand { get; set; }
        public string Id { get; set; }
        public string CategoryId { get; set; }
        public string Description { get; set; }
        public string Subcategory { get; set; }
        public double Price { get; set; }
        public int Rate { get; set; }
    }
}
