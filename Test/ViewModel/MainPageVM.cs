﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Test.ViewModel
{
    public class MainPageVM : BaseViewModel
    {
        public MainPageVM()
        {
            _categories = new ObservableCollection<ImageNameViewModel>();
        }

        private ObservableCollection<ImageNameViewModel> _categories;

        public ObservableCollection<ImageNameViewModel> Categories
        {
            get { return _categories; }
            set { _categories = value; RaisePropertyChaged("Categories"); }
        }

    }
}
