﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Test.ViewModel
{
    public class ComplexObjectListVM : BaseViewModel
    {
        private string _id;

        public string Id
        {
            get { return _id; }
            set { _id = value; RaisePropertyChaged("Id"); }
        }
        

        private string _line1;

        public string Line1
        {
            get { return _line1; }
            set { _line1 = value; RaisePropertyChaged("Line1"); }
        }

        private string _line2;

        public string Line2
        {
            get { return _line2; }
            set { _line2 = value; RaisePropertyChaged("Line2"); }
        }

        private int _value1;

        public int Value1
        {
            get { return _value1; }
            set { _value1 = value; RaisePropertyChaged("Value1"); }
        }

        private double _value2;

        public double Value2
        {
            get { return _value2; }
            set { _value2 = value; RaisePropertyChaged("Value2"); }
        }

    }
}
