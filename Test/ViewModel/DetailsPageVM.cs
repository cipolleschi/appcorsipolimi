﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Test.ViewModel
{
    public class DetailsPageVM : BaseViewModel
    {
        private string _name;

        public string Name
        {
            get { return _name; }
            set { _name = value; RaisePropertyChaged("Name"); }
        }

        private string _model;

        public string Model
        {
            get { return _model; }
            set { _model = value; RaisePropertyChaged("Model"); }
        }

        private string _brand;

        public string Brand
        {
            get { return _brand; }
            set { _brand = value; RaisePropertyChaged("Brand"); }
        }

        private string _description;

        public string Description
        {
            get { return _description; }
            set { _description = value; RaisePropertyChaged("Description"); }
        }

        private string _subcategory;

        public string Subcategory
        {
            get { return _subcategory; }
            set { _subcategory = value; RaisePropertyChaged("Subcategory"); }
        }

        private double _price;

        public double Price
        {
            get { return _price; }
            set { _price = value; RaisePropertyChaged("Price"); }
        }

        private int rating;

        public int Rating
        {
            get { return rating; }
            set { rating = value; RaisePropertyChaged("Rating"); }
        }
        
        
        

        private int _currentRating;

        public int CurrentRating
        {
            get { return _currentRating; }
            set { _currentRating = value; RaisePropertyChaged("CurrentRating"); }
        }

    }
}
