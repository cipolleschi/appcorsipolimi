﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Test.ViewModel
{
    public class ProductsPageVM : BaseViewModel
    {
        public ProductsPageVM()
        {
            _products = new ObservableCollection<ComplexObjectListVM>();
        }
        
        private string _category;

        public string Category
        {
            get { return _category; }
            set { _category = value; RaisePropertyChaged("Category"); }
        }

        private ObservableCollection<ComplexObjectListVM> _products;

        public ObservableCollection<ComplexObjectListVM> Products
        {
            get { return _products; }
            set { _products = value; RaisePropertyChaged("Products"); }
        }

    }
}
