﻿using Test.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Test.Models;

namespace Test.ViewModel
{
    public class ImageNameViewModel : BaseViewModel
    {

        public ImageNameViewModel()
        {

        }

        public ImageNameViewModel(Category c)
        {
            _name = c.Name;
            _pic = c.Image;
            _tag = c.Id;
        }

        public ImageNameViewModel(string name, string pic, string tag)
        {
            _name = name;
            _pic = pic;
            _tag = tag;
        }

        private string _tag;

        public string Tag
        {
            get { return _tag; }
            set { _tag = value; RaisePropertyChaged("Tag"); }
        }
        

        private string _name;

        public string Name
        {
            get { return _name; }
            set { _name = value; RaisePropertyChaged("Name"); }
        }

        private string _pic;

        public string Pic
        {
            get { return _pic; }
            set { _pic = value; RaisePropertyChaged("Pic"); }
        }

        
    }
}
