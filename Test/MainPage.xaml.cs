﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Navigation;
using Microsoft.Phone.Controls;
using Microsoft.Phone.Shell;
using Test.Resources;
using Test.ViewModel;
using Test.Services;

namespace Test
{
    public partial class MainPage : PhoneApplicationPage
    {
        private DateTime _startTime;
        private MainPageVM _dataContext;

        // Constructor
        public MainPage()
        {
            InitializeComponent();

            // Sample code to localize the ApplicationBar
            //BuildLocalizedApplicationBar();
        }

        protected override void OnNavigatedTo(NavigationEventArgs e)
        {
            base.OnNavigatedTo(e);
            GoogleAnalytics.EasyTracker.GetTracker().SendView("MainPage");
            _startTime = DateTime.Now;
            _dataContext = new MainPageVM();
            foreach (var c in MemoryManager.Instance.Categories)
            {
                _dataContext.Categories.Add(new ImageNameViewModel(c));
            }
            DataContext = _dataContext;
        }

        protected override void OnNavigatedFrom(NavigationEventArgs e)
        {
            GoogleAnalytics.EasyTracker.GetTracker().SendTiming(DateTime.Now.Subtract(_startTime), "permanenza", "MainPage", "Navigation");
            base.OnNavigatedFrom(e);
        }

        private void Grid_Tap(object sender, System.Windows.Input.GestureEventArgs e)
        {
            Grid g = sender as Grid;
            var uriString = String.Format("/Pages/ProductsPage.xaml?category={0}", g.Tag.ToString());
            GoogleAnalytics.EasyTracker.GetTracker().SendEvent(g.Tag.ToString(), "Click", "Category Selection", 0);            
            NavigationService.Navigate(new Uri(uriString, UriKind.Relative));
        }
    }
}