﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Test.Models;

namespace Test.Services
{
    public class MemoryManager
    {
        private readonly static MemoryManager _instance = new MemoryManager();

        private MemoryManager()
        {
            Categories = new List<Category>();
            Products = new List<Product>();
        }

        public static MemoryManager Instance {
            get { return _instance; }
        }

        public List<Category> Categories { get; set; }

        public List<Product> Products { get; set; }
    }
}
