# Informazioni generali #
In questo repository sono raccolti tutti i progetti sviluppati in tecnologia .NET e C# per i corsi erogati dal Politecnico di Milano, sede di Cremona.

Al momento, all'interno del repository è presente:

* un app legata agli acquisti in-app 

* un app che implementa i google analytics

## Requisiti generali ##

* Un microsoft account: necessario per registrarsi nel Windows Phone developer center

* Un computer con installato Visual Studio 2012 o successivo

* Le SDK per Windows Phone

* Un'installazione di GIT per scaricare il codice.

Per quanto riguarda Visual Studio, è possibile installare le versioni gratuite, liberamente scaricabili a questo indirizzo:
[http://dev.windows.com/it-it/develop/download-phone-sdk](http://dev.windows.com/it-it/develop/download-phone-sdk)

Ogni app ha poi alcuni requisiti specifici. Fare riferimento alla sezione Downloads per i requisiti delle singole app.

* [Acquisti in-app](https://bitbucket.org/cipolleschi/appcorsipolimi/downloads/In-App%20Purchase.pdf)

* [Analytics per WP8](https://bitbucket.org/cipolleschi/appcorsipolimi/downloads/Analytics%20per%20WP8.pptx)

## Installazione dei progetti ##
1. Aprire un **command prompt**
2. Navigare sino ala cartella prescelta (e.g.: cd Documents/Development)
3. Utilizzare il comando: **git clone https://cipolleschi@bitbucket.org/cipolleschi/appcorsipolimi.git**
4. Lanciare la versione di **Visual Studio** installata
5. Scegliere **File -> Apri progetto/soluzione**
6. Navigare sino alla cartella in cui è stato clonato il repository
7. Scegliere il file *polimiTesting.sln*

In questo modo Visual Studio aprirà la soluzione e tutti i progetti contenuti. Per scegliere il progetto attivo, nel menù di sinistra:

1. Cliccare con il tasto destro sul progetto

2. Scegliere l'opzione *Set as StartUp project* (l'opzione ha l'icona di un ingranaggio)