﻿using InAppPurchaseTesting.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace InAppPurchaseTesting.Graphics.ViewModel
{
    public class RecipeVM : BaseViewModel
    {

        public RecipeVM()
        {

        }

        public RecipeVM(Recipe r)
        {
            this.Id = r.Id;
            this.Description = r.Description;
            this.Owned = r.Owned;
            this.Pic = r.Pic;
            this.Title = r.Title;
            this.Price = r.Price;     
        }

        private string _id;

	    public string Id
	    {
		    get { return _id;}
		    set { _id = value; RaisePropertyChanged("Id"); }
	    }

        private string _title;

        public string Title
        {
            get { return _title; }
            set { _title = value; RaisePropertyChanged("Title"); }
        }

        private string _description;

        public string Description
        {
            get { return _description; }
            set { _description = value; RaisePropertyChanged("Description"); }
        }

        private string _pic;

        public string Pic
        {
            get { return _pic; }
            set { _pic = value; RaisePropertyChanged("Pic"); }
        }

        private bool _owned;

        public bool Owned
        {
            get { return _owned; }
            set { 
                _owned = value; 
                RaisePropertyChanged("Owned");
                RaisePropertyChanged("ToBuy");
                RaisePropertyChanged("BuyOpacity");
            }
        }

        public bool ToBuy 
        {
            get { return !Owned; } 
        }

        public double BuyOpacity
        {
            get { return Owned ? 0.5 : 1; }
        }

        private string _price;

        public string Price
        {
            get { return _price; }
            set { _price = value; RaisePropertyChanged("Price"); }
        }
        

    }
}
