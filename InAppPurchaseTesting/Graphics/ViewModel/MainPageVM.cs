﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace InAppPurchaseTesting.Graphics.ViewModel
{
    public class MainPageVM : BaseViewModel
    {

        public MainPageVM()
        {
            this.Recipes = new ObservableCollection<RecipeVM>();
        }

        private ObservableCollection<RecipeVM> _recipies;

        public ObservableCollection<RecipeVM> Recipes
        {
            get { return _recipies; }
            set { _recipies = value; RaisePropertyChanged("Recipies"); }
        }

    }
}
