﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Navigation;
using Microsoft.Phone.Controls;
using Microsoft.Phone.Shell;
using InAppPurchaseTesting.Graphics.ViewModel;
using InAppPurchaseTesting.Services;

namespace InAppPurchaseTesting.Graphics
{
    public partial class DetailPage : PhoneApplicationPage
    {
        RecipeVM _dataContext;

        public DetailPage()
        {
            InitializeComponent();            
        }

        protected override void OnNavigatedTo(NavigationEventArgs e)
        {
            base.OnNavigatedTo(e);
            var id = NavigationContext.QueryString["id"];
            var rec = MemoryStorage.Instance.Recipes.Where(x => x.Id == id).SingleOrDefault();
            if (rec != null)
            {
                _dataContext = new RecipeVM(rec);
                DataContext = _dataContext;
            }

        }
    }
}