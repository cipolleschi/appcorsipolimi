﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Navigation;
using Microsoft.Phone.Controls;
using Microsoft.Phone.Shell;
using InAppPurchaseTesting.Resources;
using InAppPurchaseTesting.Graphics.ViewModel;
using InAppPurchaseTesting.Services;
using Windows.ApplicationModel.Store;
using System.Diagnostics;

namespace InAppPurchaseTesting
{
    public partial class MainPage : PhoneApplicationPage
    {
        private MainPageVM _dataContext;

        // Constructor
        public MainPage()
        {
            InitializeComponent();
            _dataContext = new MainPageVM();
            // Sample code to localize the ApplicationBar
            //BuildLocalizedApplicationBar();

            Loaded += MainPage_Loaded;
            Unloaded += MainPage_Unloaded;
        }

        private void MainPage_Unloaded(object sender, RoutedEventArgs e)
        {
            MemoryStorage.Instance.InAppDownloaded -= Instance_InAppDownloaded;
        }

        void MainPage_Loaded(object sender, RoutedEventArgs e)
        {
            MemoryStorage.Instance.InAppDownloaded += Instance_InAppDownloaded;
        }

        void Instance_InAppDownloaded(object sender, EventArgs e)
        {
            _dataContext = new MainPageVM();
            foreach (var r in MemoryStorage.Instance.Recipes)
            {
                _dataContext.Recipes.Add(new RecipeVM(r));
            }

            DataContext = _dataContext;
        }

        protected override void OnNavigatedTo(NavigationEventArgs e)
        {
            base.OnNavigatedTo(e);
            if (e.NavigationMode != NavigationMode.Back)
            {
                foreach (var r in MemoryStorage.Instance.Recipes)
                {
                    _dataContext.Recipes.Add(new RecipeVM(r));
                }

                DataContext = _dataContext;
            }
        }

        private async void BuyBtn_Click(object sender, RoutedEventArgs e)
        {            
            Button btn = sender as Button;
            string tag = btn.Tag.ToString();

            try
            {
                //Richiede l'acquisto dell'elemento con id tag,
                string receipt = await CurrentApp.RequestProductPurchaseAsync(tag, false);

                //Acquisto effettuato..
                var rec = MemoryStorage.Instance.Recipes.Where(x => x.Id == tag).SingleOrDefault();
                if (rec != null)
                {                    
                    var rec2 = _dataContext.Recipes.Where(x => x.Id == tag).SingleOrDefault();
                    rec2.Owned = rec.Owned;
                }
            }
            catch (Exception ex)
            {
                Debug.WriteLine(ex.Message);
            }
        }

        private void navigateToId(string id)
        {
            var obj = _dataContext.Recipes.Where(x => x.Id == id).SingleOrDefault();
            if (obj == null || obj.Owned == false)
            {
                MessageBox.Show("Non hai ancora acquistato questa ricetta!", "Attenzione", MessageBoxButton.OK);
            }
            else
            {
                var navStr = String.Format("/Graphics/DetailPage.xaml?id={0}", id);
                NavigationService.Navigate(new Uri(navStr, UriKind.Relative));
            }

            
        }

        private void FrameworkElement_Tap(object sender, System.Windows.Input.GestureEventArgs e)
        {
            FrameworkElement fe = sender as FrameworkElement;
            var tag = fe.Tag.ToString();
            navigateToId(tag);
        }


        // Sample code for building a localized ApplicationBar
        //private void BuildLocalizedApplicationBar()
        //{
        //    // Set the page's ApplicationBar to a new instance of ApplicationBar.
        //    ApplicationBar = new ApplicationBar();

        //    // Create a new button and set the text value to the localized string from AppResources.
        //    ApplicationBarIconButton appBarButton = new ApplicationBarIconButton(new Uri("/Assets/AppBar/appbar.add.rest.png", UriKind.Relative));
        //    appBarButton.Text = AppResources.AppBarButtonText;
        //    ApplicationBar.Buttons.Add(appBarButton);

        //    // Create a new menu item with the localized string from AppResources.
        //    ApplicationBarMenuItem appBarMenuItem = new ApplicationBarMenuItem(AppResources.AppBarMenuItemText);
        //    ApplicationBar.MenuItems.Add(appBarMenuItem);
        //}
    }
}