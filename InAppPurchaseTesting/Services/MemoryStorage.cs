﻿using InAppPurchaseTesting.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace InAppPurchaseTesting.Services
{
    public class MemoryStorage
    {
        private static readonly MemoryStorage _instance = new MemoryStorage();
        public static MemoryStorage Instance
        {
            get { return _instance; }
        }

        private MemoryStorage()
        {
            Recipes = new List<Recipe>();

            
            Recipes.Add(new Recipe()
            {
                Id = "FC530F0D-F6D1-4241-B103-05153F0051D2",
                Description = "blablablablablablablablablablablablablablablablablablablablablablablablablablabla",
                Owned = true,
                Title = "Antipasto1",
                Pic = "http://3.bp.blogspot.com/_xMri1JH0p7U/S9NKyJVVL3I/AAAAAAAAABE/CNxWSXxZkYI/s320/Scafarda.jpg",
                Price = "$0.00"
            });
            Recipes.Add(new Recipe()
            {
                Id = "CCADD249-789F-4F99-A206-764EF13C0825",
                Description = "blablablablablablablablablablablablablablablablablablablablablablablablablablabla",
                Owned = true,
                Title = "Antipasto2",
                Price = "$0.00",
                Pic = "http://ecx.images-amazon.com/images/I/61yE-%2BX2zNL._SY300_.jpg"
            });
            Recipes.Add(new Recipe()
            {
                Id = "A0EDE3A5-1FB3-462D-B619-964C243501AB",
                Description = "blablablablablablablablablablablablablablablablablablablablablablablablablablabla",
                Owned = true,
                Title = "Primo1",
                Price = "$0.00",
                Pic = "http://img4-2.myrecipes.timeinc.net/i/recipes/su/11/spice-crusted-scallops-su-l.jpg"
            });
            Recipes.Add(new Recipe()
            {
                Id = "C1F60B2F-3287-4031-AEFA-FF619FE86377",
                Description = "blablablablablablablablablablablablablablablablablablablablablablablablablablabla",
                Owned = true,
                Title = "Primo2",
                Price = "$0.00",
                Pic = "http://papasitalianrecipes.com/wp-content/uploads/2014/02/gricia-300x300.jpg"
            });
            Recipes.Add(new Recipe()
            {
                Id = "BC29F053-22CC-4F5D-9F62-7173956B6E23",
                Description = "blablablablablablablablablablablablablablablablablablablablablablablablablablabla",
                Owned = true,
                Title = "Secondo1",
                Price = "$0.00",
                Pic = "https://www.edamam.com/web-img/014/014a6464e3bd1d357f8082efdbd333dd.jpg"
            });
            
        }

        public List<Recipe> Recipes { get; set; }

        public event EventHandler InAppDownloaded;

        public void RaiseInAppDownloaded()
        {
            if (InAppDownloaded != null)
            {
                InAppDownloaded(this, new EventArgs());
            }
        }
    }
}
